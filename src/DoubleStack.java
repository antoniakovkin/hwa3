import java.util.*;

/** Stack manipulation.
 * @since 1.8
 *
 * Sources:
 * https://courses.cs.washington.edu/courses/cse326/03su/homework/hw1/DoubleStack.java
 * https://enos.itcollege.ee/~japoia/algorithms/examples/IntStack.java
 * https://www.programcreek.com/2012/12/leetcode-evaluate-reverse-polish-notation/
 * https://javahungry.blogspot.com/2020/06/check-string-contains-special-characters.html
 */
public class DoubleStack {

   private LinkedList<String> stringStack;


   public static void main (String[] argum) {
      //String test = "3. 4. + 7 *" ;
      //System.out.println(interpret(test));
      //String test1 = "   \t \t356.  \t \t";
      //System.out.println(interpret(test));
      //DoubleStack m = new DoubleStack();
//      m.push (-8.5);
//      m.push (7.14);
//      m.push (2.73);
//      DoubleStack m2 = null;
//      try {
//         m2 = (DoubleStack)m.clone();
//      } catch (CloneNotSupportedException e){}
//      String s1 = m.toString().substring (0, 8);
//      System.out.println(s1);
//      m.push (2.73);
//      String s2 = m.toString().substring (0, 8);
//      System.out.println(s2);
      // TODO!!! Your tests here!
   }

   DoubleStack() {
      stringStack = new LinkedList<>();
      // TODO!!! Your constructor here!
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      //String retrieve;
      DoubleStack second = new DoubleStack();
      //for(int i = stringStack.size() - 1;i > -1;i--)
      //{
      //retrieve = stringStack.get(i);
      // second.push(Double.parseDouble(retrieve));
      //}
      second.stringStack = (LinkedList<String>) stringStack.clone();
      return second; // TODO!!! Your code here!
   }

   public boolean stEmpty() {

      if(stringStack.isEmpty())
      {
         return true;
      }
      else {
         return false;
      }
      // TODO!!! Your code here!
   }

   public void push (double a) {
      stringStack.addFirst(Double.toString(a));
      // TODO!!! Your code here!
   }

   public double pop() {
      if(stEmpty())
      {
         throw new IndexOutOfBoundsException(" stack underflow ");
      }
      String convert = stringStack.pop();
      return Double.parseDouble(convert);
      //return 0.; // TODO!!! Your code here!
   } // pop

   public void op (String s) {
      if (stringStack.size() < 2)
         throw new IndexOutOfBoundsException("Too few elements to " + s);
      double operand1 = pop();
      double operand2 = pop();
      if ("+".equals(s)) {
         push(operand1 + operand2);
      } else if ("-".equals(s)) {
         push(operand2 - operand1);
      } else if ("*".equals(s)) {
         push(operand1 * operand2);
      } else if ("/".equals(s)) {
         push(operand2 / operand1);
      } else {
         throw new IllegalArgumentException("Invalid operation: " + s);
      }
   }

   public double tos() {
      if(stEmpty())
      {
         throw new IndexOutOfBoundsException(" stack overflow");
      }
      return Double.parseDouble(stringStack.getFirst());
      //return 0.; // TODO!!! Your code here!
   }

   @Override
   public boolean equals (Object o) {
      if (((DoubleStack) o).stringStack.size() != stringStack.size())
      {
         return false;
      }
      for(int i = stringStack.size() - 1;i > -1;i--)
         if (!((DoubleStack) o).stringStack.get(i).equals(stringStack.get(i))) {
            return false;
         }
      return true; // TODO!!! Your code here!
   }

   @Override
   public String toString() {
      if (stEmpty())
      {
         return "empty";
      }
      StringBuffer buffer = new StringBuffer();
      for(int i = stringStack.size() - 1;i > -1;i--)
      {
         buffer.append(stringStack.get(i)).append(" ");
      }
      return buffer.toString(); // TODO!!! Your code here!
   }

   public static double interpret (String pol) {
      DoubleStack stack = new DoubleStack();
      if(pol.trim().isEmpty())
      {
         throw new RuntimeException(pol.trim() + "Input string is empty");
      }
      String[] calculate = pol.trim().split("\\s+");
      String illegalCharacters = "!@#$%&()',:;<=>?[]^_`{|}";
      for(int i = 0; i < pol.trim().length();i++) {
         char symbol = pol.trim().charAt(i);

         if (illegalCharacters.contains(Character.toString(symbol))) {
            throw new RuntimeException(pol.trim() + " expression has illegal symbol " + symbol);
         }
      }
      double result;
      double x,y,z;
      int operandCount=0;
      int operatorCount=0;
      String operators = "+-*/";
      String SWAP = "SWAP";
      String DUP = "DUP";
      String ROT = "ROT";
      String more_operators ="SWAP DUP ROT";
      for(String t : calculate)
      {
         if(!operators.contains((t)) && !more_operators.contains(t))
         {
               stack.push(Double.parseDouble(t));
               operandCount++;
         }
         else{
            if(t.equals(SWAP))
            {
               if(operandCount < 2) {
                  throw new IndexOutOfBoundsException(pol.trim() + " only has " + operandCount +" elements, too few for SWAP");
               }
                  x = stack.pop();
                  y = stack.pop();
                  stack.push(x);
                  stack.push(y);
            }
            else if(t.equals(DUP))
            {
               if(operandCount < 1) {
                  throw new IndexOutOfBoundsException(pol.trim() + " for DUP needs at least 1 element, you have:" + operandCount);
               }
               x = stack.tos();
               stack.push(x);
               operandCount++;
            }
            else if(t.equals(ROT))
            {
               if(operandCount < 3) {
                  throw new IndexOutOfBoundsException(pol.trim() + " for ROT needs at least 3 element, you have:" + operandCount);
               }
               x = stack.pop();
               y = stack.pop();
               z = stack.pop();
               stack.push(y);
               stack.push(x);
               stack.push(z);
            }
            else {
               if(stack.stringStack.size() < 2)
               {
                  throw new IndexOutOfBoundsException(pol.trim() + " has not enough number to " + t);
               }
               stack.op(t);
               operatorCount++;
            }
         }
      }
      if(operandCount - operatorCount != 1)
      {
         throw new RuntimeException("Expression " + pol.trim() +" must have one less operator than operands, you have: operands - "+ operandCount + " operators - " + operatorCount);
      }
      else {
         result = stack.pop();
         return result;
      }// TODO!!! Your code with full error handling here!
   }

}

